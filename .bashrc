# .bashrc

# If not running interactively, exit
[[ $- != *i* ]] && return


# Command prompt
export PS1="\[\e[32m\]\W \\$ \[\e[m\]"


# Custom aliases
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias diff='diff --color=auto'

alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

alias dotfiles='/usr/bin/git --git-dir=/home/b-erhart/.config/dotfiles --work-tree=/home/b-erhart'
