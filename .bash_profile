# .bash_profile

# XDG Base Directory
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share

# Custom environment variables
export PATH=$PATH:$HOME/.local/bin

# Variables to clean up $HOME
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export VIMINIT="source $XDG_CONFIG_HOME/vim/vimrc"
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc

# Bash-related variables
export HISTFILE="$XDG_DATA_HOME"/bash/history
export HISTFILESIZE=65536
export HISTSIZE=${HISTFILESIZE}
